/* 
 * The MIT License (MIT)
 * 
 * Copyright (c) 2016 Tellerva, Marc Lawrence
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package me.scarlet.undertailor.texts;

import com.badlogic.gdx.graphics.Color;
import com.google.common.base.Preconditions;
import me.scarlet.undertailor.audio.SoundWrapper;
import me.scarlet.undertailor.util.Pair;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class TextComponent {
    
    public static class DisplayMeta {
        
        public static DisplayMeta defaults() {
            return new DisplayMeta();
        }
        
        public float offX, offY, scaleX, scaleY;
        public Color color;
        
        public DisplayMeta() {
            this(0, 0, 1.0F, 1.0F, null);
        }
        
        public DisplayMeta(float offX, float offY, float scaleX, float scaleY, Color color) {
            this.offX = offX;
            this.offY = offY;
            this.scaleX = scaleX;
            this.scaleY = scaleX;
            this.color = color;
        }
        
        public String toString() {
            return "[" + offX + ", " + offY + ", " + scaleX + ", " + scaleY + "]";
        }
    }
    
    public static class Text extends TextComponent {
        private List<TextComponent> members;
        
        public Text(Font font) {
            this(font, null);
        }
        
        public Text(Font font, Style style) {
            this(font, style, Color.WHITE);
        }
        
        public Text(Font font, Style style, Color color) {
            this(font, style, color, null);
        }
        
        public Text(Font font, Style style, Color color, SoundWrapper sound) {
            this(font, style, color, sound, DEFAULT_SPEED);
        }
        
        public Text(Font font, Style style, Color color, SoundWrapper sound, Integer speed) {
            this(font, style, color, sound, speed, 1);
        }
        
        public Text(Font font, Style style, Color color, SoundWrapper sound, Integer speed, Integer segmentSize) {
            this(font, style, color, sound, speed, segmentSize, 0F);
        }
        
        public Text(Font font, Style style, Color color, SoundWrapper sound, Integer speed, Integer segmentSize, Float wait, TextComponent... components) {
            super(null, font, style, color, sound, speed, segmentSize, wait);
            this.members = new ArrayList<>();
            
            for(TextComponent component : components) {
                component.parent = this;
                members.add(component);
            }
        }
        
        @Override
        public String getText() {
            StringBuilder sb = new StringBuilder();
            members.forEach(com -> {
                sb.append(com.getText());
            });
            
            return sb.toString();
        }
        
        public List<TextComponent> getMembers() {
            return new ArrayList<>(members);
        }
        
        public void addComponents(TextComponent... components) {
            for(TextComponent component : components) {
                component.parent = this;
                members.add(component);
            }
        }
        
        public TextComponent getComponentAtCharacter(int chara) {
            Preconditions.checkArgument(!members.isEmpty(), "no members within this Text object");
            Preconditions.checkArgument(this.getText().length() >= chara || chara < 0, "char index out of bounds");
            
            if(members.size() == 1) {
                return members.get(0);
            }
            
            List<Pair<TextComponent, Integer>> compList = new ArrayList<>();
            for(int i = 0; i < members.size(); i++) {
                TextComponent member = members.get(i);
                int len = member.getText().length();
                if(!compList.isEmpty()) {
                    len = len + compList.get(compList.size() - 1).getFirstElement().get().getText().length();
                }
                
                compList.add(new Pair<>(member, len));
            }
            
            for(int i = 0; i < compList.size(); i++) {
                if(i == 0) {
                    continue;
                }
                
                if(compList.get(i - 1).getSecondElement().get() > chara && chara <= compList.get(i).getSecondElement().get()) {
                    return compList.get(i - 1).getFirstElement().get();
                }
            }
            
            return compList.get(compList.size() - 1).getFirstElement().get();
        }
        
        @Override
        public Text substring(int start) {
            return substring(start, getText().length());
        }
        
        @Override
        public Text substring(int start, int end) {
            if(start == end) {
                return new Text(super.font, super.style, super.color, super.textSound, super.speed, super.segmentSize, super.wait,
                        new TextComponent("", super.font, super.style, super.color, super.textSound, super.speed, super.segmentSize, super.wait));
            }
            
            if(members.size() == 1) {
                return new Text(super.font, super.style, super.color, super.textSound, super.speed, super.segmentSize, super.wait, members.get(0).substring(start, end));
            }
            
            Map<TextComponent, Integer> compMap = new LinkedHashMap<>();
            
            TextComponent last = null;
            for(int i = 0; i < members.size(); i++) {
                TextComponent member = members.get(i);
                int len = member.getText().length();
                if(last != null) {
                    len = len + compMap.get(last);
                }
                
                last = member;
                compMap.put(member, len);
            }
            
            List<TextComponent> compList = new ArrayList<>();
            Entry<TextComponent, Integer> previous = null;
            int processed = 0;
            for(Entry<TextComponent, Integer> entry : compMap.entrySet()){
                TextComponent added = null;
                if(compList.isEmpty()) { // first one
                    if(start < entry.getValue()) {
                        int val = processed == 0 ? start : start - previous.getValue();
                        added = entry.getKey().substring(val);
                    }
                } else {
                    added = entry.getKey();
                    if(end <= entry.getValue()) {
                        if(end < previous.getValue()) {
                            int lastIndex = compList.size() - 1;
                            TextComponent comp = compList.get(lastIndex);
                            compList.remove(lastIndex);
                            if(processed == 1) {
                                compList.add(comp.substring(start, end));
                            } else {
                                compList.add(comp.substring(0, previous.getValue() - end));
                            }
                            
                            break;
                        } else {
                            int val = end - previous.getValue();
                            
                            added = entry.getKey().substring(0, val);
                            compList.add(added);
                            break;
                        }
                    }
                }
                
                if(added != null) {
                    compList.add(added);
                }
                
                previous = entry;
                processed++;
            }
            
            return new Text(super.font, super.style, super.color, super.textSound, super.speed, super.segmentSize, super.wait, compList.toArray(new TextComponent[compList.size()]));
        }
    }
    
    private TextComponent parent;
    private SoundWrapper textSound;
    private String text;
    private Color color;
    private Style style;
    private Font font;
    private Integer speed;       // segments per second?
    private Integer segmentSize; // how many characters to play at a time
    private Float wait;          // delay between text components
    
    public static final int DEFAULT_SPEED = 35;
    
    public static TextComponent of(String text, Font font) {
        return new TextComponent(text, font);
    }
    
    public TextComponent(String text, Font font) {
        this(text, font, null);
    }
    
    public TextComponent(String text, Font font, Style style) {
        this(text, font, style, null);
    }
    public TextComponent(String text, Font font, Style style, Color color) {
        this(text, font, style, color, null);
    }
    
    public TextComponent(String text, Font font, Style style, Color color, SoundWrapper textSound) {
        this(text, font, style, color, textSound, DEFAULT_SPEED);
    }
    
    public TextComponent(String text, Font font, Style style, Color color, SoundWrapper textSound, Integer speed) {
        this(text, font, style, color, textSound, speed, 1);
    }
    
    public TextComponent(String text, Font font, Style style, Color color, SoundWrapper textSound, Integer speed, Integer segmentSize) {
        this(text, font, style, color, textSound, speed, segmentSize, 0F);
    }
    
    public TextComponent(String text, Font font, Style style, Color color, SoundWrapper textSound, Integer speed, Integer segmentSize, Float wait) {
        this.text = text;
        this.textSound =  textSound;
        this.color = color;
        this.speed = speed;
        this.wait = wait;
        this.style = style;
        this.font = font;
        this.segmentSize = segmentSize;
        
        if(speed != null && speed <= 0) {
            speed = DEFAULT_SPEED;
        }
        
        if(segmentSize != null && segmentSize <= 0) {
            segmentSize = 1;
        }
        
        if(wait != null && wait < 0) {
            wait = 0F;
        }
    }
    
    public String getText() {
        return text;
    }
    
    public Color getColor() {
        if(color == null) {
            if(parent != null && parent.color != null) {
                return parent.color;
            }
            
            return Color.WHITE;
        }
        
        return color;
    }
    
    public SoundWrapper getSound() {
        if(textSound == null && parent != null) {
            return parent.textSound;
        }
        
        return textSound;
    }
    
    public int getSpeed() {
        if(speed == null) {
            if(parent != null && parent.speed != null) {
                return parent.speed;
            }
            
            return DEFAULT_SPEED;
        }
        
        return speed;
    }
    
    public Style getStyle() {
        if(style == null && parent != null) {
            return parent.style;
        }
        
        return style;
    }
    
    public Font getFont() {
        if(font == null && parent != null) {
            return parent.font;
        }
        
        return font;
    }
    
    public float getDelay() {
        if(wait == null) {
            if(parent != null && parent.wait != null) {
                return parent.wait;
            }
            
            return 0F;
        }
        
        return wait;
    }
    
    public int getSegmentSize() {
        if(segmentSize == null) {
            if(parent != null && parent.segmentSize != null) {
                return parent.segmentSize;
            }
            
            return 1;
        }
        
        return segmentSize;
    }
    
    public TextComponent substring(int start) {
        return substring(start, this.text.length());
    }
    
    public TextComponent substring(int start, int end) {
        return new TextComponent(text.substring(start, end), font, style, color, textSound, speed, segmentSize, wait);
    }
}
